package com.bill.calculator.domain;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PricingEngineTest {

    PricingEngine pe = new PricingEngine();

    @Test
    public void getPriceTo1SecondCall() {
        Duration duration = new Duration(0, 0, 1);
        Integer price = pe.getTotalPrice(duration);
        assertEquals(3, price.intValue());
    }

    @Test
    public void getPriceTo1MinuteCall() {
        Duration duration = new Duration(0, 1, 0);
        Integer price = pe.getTotalPrice(duration);
        assertEquals(180, price.intValue());
    }

    @Test
    public void getPriceTo1HourCall() {
        Duration duration = new Duration(1, 0, 0);
        Integer price = pe.getTotalPrice(duration);
        assertEquals(9000, price.intValue());
    }

    @Test
    public void getPriceTo5MinutesCall() {
        Duration duration = new Duration(0, 5, 0);
        Integer price = pe.getTotalPrice(duration);
        assertEquals(750, price.intValue());
    }

    @Test
    public void getPriceTo5Minutes1SecondCall() {
        Duration duration = new Duration(0, 5, 1);
        Integer price = pe.getTotalPrice(duration);
        assertEquals(900, price.intValue());
    }

    @Test
    public void getPriceTo4Minutes59SecondsCall() {
        Duration duration = new Duration(0, 4, 59);
        Integer price = pe.getTotalPrice(duration);
        assertEquals(897, price.intValue());
    }

    @Test
    public void getPriceTo5Minutes59SecondsCall() {
        Duration duration = new Duration(0, 5, 59);
        Integer price = pe.getTotalPrice(duration);
        assertEquals(900, price.intValue());
    }

    @Test
    public void getPriceTo1Hour1Minute1SecondCall() {
        Duration duration = new Duration(1, 1, 1);
        Integer price = pe.getTotalPrice(duration);
        assertEquals(9300, price.intValue());
    }
}

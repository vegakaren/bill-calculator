package com.bill.calculator.domain;

import io.vavr.collection.List;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BillCalculatorTest {

    @Test
    public void calculateBillFromString() {
        String billInString = new StringBuilder("00:01:07,400-234-090\n")
                .append("00:05:01,701-080-080\n")
                .append("00:05:00,400-234-090\n")
                .toString();
        Bill bill = new Bill(billInString);
        bill.priceBill();
        Integer billValue = bill.applyPromotions();
        assertEquals(900, billValue.intValue());
    }

    @Test
    public void calculateBillFromStringLines() {
        String s1 = "01:01:01,310-356-789";
        String s2 = "00:04:59,320-356-789";
        String s3 = "00:05:59,309-356-789";
        String s4 = "00:05:00,310-356-789";
        String s5 = "00:04:00,320-356-789";
        Bill bill = new Bill(List.of(s1, s2, s3, s4, s5));
        bill.priceBill();
        Integer billValue = bill.applyPromotions();
        assertEquals(2517, billValue.intValue());
    }
}

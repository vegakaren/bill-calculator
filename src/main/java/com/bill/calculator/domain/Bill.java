package com.bill.calculator.domain;

import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.collection.List;

public class Bill {

    private List<Call> calls;
    private List<Call> callsWithPrices = List.empty();

    public Bill(String lines) {
        this(List.of(lines.split("\n")));
    }

    public Bill(List<String> lines) {
        this.calls = lines.map(line -> {
            Tuple2<String, Integer> phoneAndTime = parseLine(line);
            Duration duration = new Duration(phoneAndTime._1, ":");
            return new Call(duration, phoneAndTime._2);
        });
    }

    public void priceBill() {
        PricingEngine pe = new PricingEngine();
        this.callsWithPrices = calls.map(c -> new Call(c.duration, c.number, pe.getTotalPrice(c.duration)));

    }

    public Integer applyPromotions() {
        PromotionsEngine pre = new PromotionsEngine();
        List<Call> callsWithPromotionsApplied = pre.applyPromotions(callsWithPrices);
        Integer price = callsWithPromotionsApplied.foldRight(0, (call, sum) -> call.price.get() + sum);
        return price;
    }

    private static Tuple2<String, Integer> parseLine(String line) {
        String[] durationAndPhone = line.split(",");
        String phoneFormatted = durationAndPhone[1].replaceAll("-","");
        return Tuple.of(durationAndPhone[0], Integer.valueOf(phoneFormatted));
    }

}

package com.bill.calculator.util;

import io.vavr.collection.List;
import io.vavr.control.Either;

public interface Reader {

    Either<String, List<String>> readInput();
}

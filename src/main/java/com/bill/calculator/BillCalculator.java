package com.bill.calculator;

import com.bill.calculator.domain.Bill;
import com.bill.calculator.util.FileReader;
import com.bill.calculator.util.Reader;
import io.vavr.collection.List;
import io.vavr.control.Either;


class BillCalculator {

    public static void main(String[] args) {
        Reader reader = new FileReader();
        Either<String, List<String>> readInput = reader.readInput();
        Either<String, Bill> maybeBill = readInput.map(lines -> new Bill(lines));
        maybeBill.map(b -> {
            b.priceBill();
            System.out.println(b.applyPromotions());
            return "Fin";
        });
    }



}

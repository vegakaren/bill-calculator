package com.bill.calculator.util;

import io.vavr.collection.List;
import io.vavr.control.Either;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileReader implements Reader {
    @Override
    public Either<String, List<String>> readInput() {
        File file = new File("resources/calls.txt");
        try {
            Scanner sc = new Scanner(file);
            List<String> lines = List.empty();
            while (sc.hasNext()) {
                String line = sc.nextLine();
                lines = lines.append(line);
            }
            return Either.right(lines);
        } catch (FileNotFoundException e) {
            return Either.left("No se encontró el archivo");
        }
    }
}

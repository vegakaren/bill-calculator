package com.bill.calculator.domain;


import io.vavr.control.Option;

public class Call {

    Duration duration;
    Integer number;
    Option<Integer> price;

    public Call(Duration duration, Integer number) {
        this.duration = duration;
        this.number = number;
        this.price = Option.none();
    }

    public Call(Duration duration, Integer number, Integer price) {
        this.duration = duration;
        this.number = number;
        this.price = Option.some(price);
    }
}

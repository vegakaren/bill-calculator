package com.bill.calculator.domain;

class Duration {

    Integer hours;
    Integer minutes;
    Integer seconds;

    public Duration(String line, String formatter) {
        String[] lineSplit = line.split(formatter);
        if (lineSplit.length == 3) {
            this.hours = Integer.valueOf(lineSplit[0]);
            this.minutes = Integer.valueOf(lineSplit[1]);
            this.seconds = Integer.valueOf(lineSplit[2]);
        } else {
            new Duration(0, 0, 0);
        }
    }

    public Duration(Integer hours, Integer minutes, Integer seconds) {
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
    }

    public Integer getTotalSeconds() {
        return (hours * 60 * 60) + (minutes * 60) + seconds;
    }
    public Integer getAllStartedMinutes() {
        Integer totalMinutes = (hours * 60) + minutes;
        if (seconds > 0) {
            return totalMinutes + 1;
        }
        return totalMinutes;
    }


}

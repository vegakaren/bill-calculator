package com.bill.calculator.domain;

import io.vavr.collection.List;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PromotionsEngineTest {

    PromotionsEngine pe = new PromotionsEngine();

    @Test
    public void applyPromotion() {
        Call call1 = new Call(new Duration(1, 1, 1), 310356789, 9300);
        Call call2 = new Call(new Duration(0, 4, 59), 320356789, 897);
        Call call3 = new Call(new Duration(0, 5, 59), 309356789, 900);
        Call call4 = new Call(new Duration(0, 5, 0), 310356789, 750);
        Call call5 = new Call(new Duration(0, 4, 0), 320356789, 720);
        List<Call> calls = List.of(call1, call2, call3, call4, call5);
        List<Call> appliedPromotions = pe.applyPromotions(calls);
        assertEquals(List.of(call2, call3, call5), appliedPromotions);
    }

    @Test
    public void applyPromotionWithTie() {
        Call call1 = new Call(new Duration(1, 1, 1), 310356789, 9300);
        Call call2 = new Call(new Duration(0, 4, 59), 320356789, 897);
        Call call3 = new Call(new Duration(1, 5, 1), 309356789, 9900);
        Call call4 = new Call(new Duration(0, 5, 0), 320356789, 750);
        Call call5 = new Call(new Duration(0, 4, 0), 310356789, 720);
        List<Call> calls = List.of(call1, call2, call3, call4, call5);
        List<Call> appliedPromotions = pe.applyPromotions(calls);
        assertEquals(List.of(call1, call2, call4, call5), appliedPromotions);
    }
}

package com.bill.calculator.domain;

import io.vavr.collection.List;
import io.vavr.collection.Map;

public class PromotionsEngine {

    public List<Call> applyPromotions(List<Call> calls) {
        Map<Integer, Integer> totalDurationByNumber = groupCallsByNumber(calls);
        Integer freeCallsNumber = getFreeCallsNumber(totalDurationByNumber);
        return calls.filter(c -> c.number.intValue() != freeCallsNumber.intValue());
    }

    private Integer getFreeCallsNumber(Map<Integer, Integer> totalDurationByNumber) {
        Integer longestDuration = getLongestTotalDuration(totalDurationByNumber);
        Map<Integer, Integer> allLongestCalls = getAllLongestCalls(totalDurationByNumber, longestDuration);
        if (allLongestCalls.size() > 1) {
            return getSmallNumber(allLongestCalls.keySet().toList());
        }
        return allLongestCalls.head()._1;
    }

    private Integer getLongestTotalDuration(Map<Integer, Integer> totalDurationByNumber) {
       return totalDurationByNumber.maxBy(c -> c._2).get()._2;
    }

    private Map<Integer, Integer> groupCallsByNumber(List<Call> calls) {
        Map<Integer, List<Call>> callsByNumber = calls.groupBy(c -> c.number);
        return callsByNumber.mapValues(c -> c.foldRight(0, (call, sum) -> call.duration.getTotalSeconds() + sum));
    }

    private Map<Integer, Integer> getAllLongestCalls(Map<Integer, Integer> totalDurationByNumber, Integer maxValue) {
        return totalDurationByNumber.filter(c -> c._2.intValue() == maxValue);
    }

    private Integer getSmallNumber(List<Integer> allCallNumbers) {
        return allCallNumbers.min().get();
    }
}

package com.bill.calculator.domain;

public class PricingEngine {

    public Integer getTotalPrice(Duration duration) {
        if (duration.minutes > 4 || duration.hours > 0) {
            return calculateByMinute(duration);
        } else {
            return calculateBySecond(duration);
        }
    }

    private Integer calculateByMinute(Duration duration) {
        return  duration.getAllStartedMinutes() * 150;
    }

    private Integer calculateBySecond(Duration duration) {
        return duration.getTotalSeconds() * 3;
    }
}
